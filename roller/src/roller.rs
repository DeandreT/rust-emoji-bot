use pest::{
    iterators::{Pair, Pairs},
    prec_climber::{PrecClimber, Operator, Assoc},
    Parser,
};
use rand::distributions::{Distribution, Uniform};

#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct RollParser;

lazy_static! {
    static ref PREC_CLIMBER: PrecClimber<Rule> = {
        use Assoc::*;
        use Rule::*;

        // precedence: (addition = subtraction) < multiplication
        PrecClimber::new(vec![
            Operator::new(add, Left) | Operator::new(subtract, Left),
            Operator::new(multiply, Left),
        ])
    };
}

fn eval(expression: Pairs<Rule>) -> i128 {
    PREC_CLIMBER.climb(
        expression,
        |pair: Pair<Rule>| match pair.as_rule() {
            Rule::natural => pair.as_str().parse::<i128>().unwrap(),
            Rule::roll => {
                let mut inner = pair.into_inner();
                let qty = inner.next().unwrap().as_str().parse::<i128>().unwrap();
                let sides = inner.next().unwrap().as_str().parse::<i128>().unwrap();
                roll_rng(qty, sides)
            },
            Rule::expr => eval(pair.into_inner()),
            _ => unreachable!(),
        },
        |lhs: i128, binop: Pair<Rule>, rhs: i128| match binop.as_rule() {
            Rule::add      => lhs + rhs,
            Rule::subtract => lhs - rhs,
            Rule::multiply => lhs * rhs,
            _ => unreachable!(),
        }
    )
}

fn roll_rng(qty: i128, sides: i128) -> i128 {
    let mut rng = rand::thread_rng();
    let die = Uniform::from(1..sides+1);

    let mut total: i128 = 0;
    for _ in 0..qty {
        total += die.sample(&mut rng);
    }

    return total;
}

pub fn dieroll(str: &str) -> Option<i128> {
    RollParser::parse(Rule::command, str).ok().map(|val| eval(val))
}
