use chrono::prelude::*;
use rand::prelude::*;
use regex::Regex;
use serenity::{
    client::Context,
    framework::standard::{
        macros::{command, group},
        Args, CommandResult,
    },
    model::channel::Message,
    utils::{content_safe, ContentSafeOptions},
};
use sysinfo::SystemExt;
use roller::roller::dieroll;

#[group]
#[commands(roll, uwu, blursed, clap, diceroll)]
struct General;

#[group]
#[owners_only]
#[commands(unixtime, cpu, mem)]
struct Owner;

#[command]
fn clap(ctx: &mut Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut clap_text = String::new();
    if !args.is_empty() {
        let args_len = args.len();
        for (i, arg) in args.iter::<String>().enumerate() {
            let arg = arg.unwrap_or("".to_string());
            if i == args_len - 1 {
                clap_text += &arg;
                break;
            } else {
                clap_text += &(arg + ":clap:")
            }
        }
    } else {
        let wmatch = Regex::new(r"(\b\s)").unwrap();
        let msg_content = content_safe(
            &ctx.cache,
            &msg.channel_id
                .messages(&ctx.http, |r| r.before(&msg.id).limit(1))
                .expect("Couldn't get previous message")[0]
                .content,
            &ContentSafeOptions::default(),
        );
        clap_text = wmatch.replace_all(&msg_content, " :clap: ").to_string();
    }
    if clap_text.contains(":clap:") {
        msg.channel_id
            .say(&ctx.http, clap_text)
            .expect("Can't send clap message");
    }
    Ok(())
}

#[command]
fn blursed(ctx: &mut Context, msg: &Message) -> CommandResult {
    let msg_content = content_safe(
        &ctx.cache,
        &msg.channel_id
            .messages(&ctx.http, |r| r.before(&msg.id).limit(1))
            .expect("Couldn't get previous message")[0]
            .content,
        &ContentSafeOptions::default(),
    );
    let mut msg_content: Vec<String> = msg_content
        .split_whitespace()
        .map(|x| x.to_string())
        .collect();
    for word in &mut msg_content {
        if word.starts_with(":") && word.ends_with(":") {
            continue
        } else {
            word.remove(0);
            word.insert_str(0, ":b:");
        }
    }
    msg.channel_id
        .say(&ctx.http, msg_content.join(" "))
        .expect("Can't send blursed message");
    Ok(())
}

#[command]
fn uwu(ctx: &mut Context, msg: &Message) -> CommandResult {
    let emoji_match = Regex::new(r"(:[\w]+:)").unwrap();
    let base_words = [
        "that", "That", "this", "This", "them", "Them", "the", "The", "r", "R", "l", "L",
    ];
    let substitutes = [
        "dat", "Dat", "dis", "Dis", "dem", "Dem", "de", "De", "w", "W", "w", "W",
    ];
    let msg_content = content_safe(
        &ctx.cache,
        msg.channel_id
            .messages(&ctx.http, |r| r.before(&msg.id).limit(1))
            .expect("Couldn't get previous message")[0]
            .content
            .clone(),
        &ContentSafeOptions::default(),
    );
    let mut msg_words: Vec<String> = msg_content
        .split_whitespace()
        .map(|x| x.to_string())
        .collect();
    for word in &mut msg_words {
        if emoji_match.is_match(word) {
            continue
        } else {
            for (i, base) in base_words.iter().enumerate() {
                if word.contains(base) {
                    *word = word.replace(base, substitutes[i]);
                    break
                }
            }
        }
    }
    msg.channel_id
        .say(&ctx.http, msg_words.join(" "))
        .expect("Can't send uwu message");
    Ok(())
}

#[command]
fn unixtime(ctx: &mut Context, msg: &Message) -> CommandResult {
    let current_time = Utc::now().timestamp();
    msg.channel_id
        .say(&ctx.http, format!("{}", current_time))
        .expect("Can't send unix timestamp");
    Ok(())
}

#[command]
fn mem(ctx: &mut Context, msg: &Message) -> CommandResult {
    let mut system = sysinfo::System::new_all();
    system.refresh_all();
    let total_mem = system.get_total_memory() / 1000;
    let used_mem = system.get_used_memory() / 1000;
    let mem_pcnt = used_mem as f32 / total_mem as f32 * 100.0;
    msg.channel_id
        .say(
            &ctx.http,
            format!("{}MB/{}MB used ({:.1}%)", used_mem, total_mem, mem_pcnt),
        )
        .expect("Can't send memory info");
    Ok(())
}

#[command]
fn cpu(ctx: &mut Context, msg: &Message) -> CommandResult {
    let mut system = sysinfo::System::new_all();
    system.refresh_all();
    let load_avg = system.get_load_average();
    msg.channel_id
        .say(
            &ctx.http,
            format!(
                "1m: {}% 5m: {}%, 15m: {}% - Load Averages",
                load_avg.one, load_avg.five, load_avg.fifteen
            ),
        )
        .expect("Can't send CPU info");
    Ok(())
}

#[command]
fn roll(ctx: &mut Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut max = args.single::<i128>().unwrap_or(100);
    let mut min = args.single::<i128>().unwrap_or(1);

    if min > max {
        std::mem::swap(&mut min, &mut max);
    }

    let outcome = rand::thread_rng().gen_range(min, max);
    msg.channel_id
        .say(
            &ctx.http,
            format!("{} rolls {} ({} - {})", msg.author.name, outcome, min, max),
        )
        .expect("Can't send roll message");
    Ok(())
}

#[command]
#[aliases(r, rolldice)]
fn diceroll(ctx: &mut Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut content = Vec::new();
    for arg in args.iter() {
        let mut roll: String = arg.unwrap();
        if roll.starts_with("d") {
            roll.insert_str(0, "1");
        }
        content.push(roll);
    }
    if let Some(total) = dieroll(&content.join(" ")) {
        msg.channel_id
            .say(
                &ctx.http,
                format!("{} rolls {}", msg.author.name, total)
                )
            .expect("Can't send dice roll message");
    } else {
        msg.channel_id
            .say(
                &ctx.http,
                format!("Invalid roll command")
                )
            .expect("Can't send dice roll message");
    }
    Ok(())
}
