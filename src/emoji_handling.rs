use regex::Regex;
use serenity::{client::Context, model::channel::Message};
use std::{
    fs::{File, OpenOptions},
    io::prelude::*,
    io::{BufReader, LineWriter},
};

fn get_local_emojis() -> Vec<String> {
    let list = File::open("emojis.list").expect("emojis.list not found");
    let buf = BufReader::new(list);
    let lines = buf.lines().map(|l| l.expect("Line unparseable")).collect();
    return lines;
}

fn emoji_dbadd(emoji: String, uid: String) {
    let ftype = "png";
    let list = OpenOptions::new()
        .append(true)
        .open("emojis.list")
        .expect("Couldn't open emoji db file");
    let mut list = LineWriter::new(list);
    list.write_all(format!("{}: {}.{}\n", emoji, uid, ftype).as_bytes())
        .expect("Couldn't write to emoji db");
}

fn post_emoji(ctx: &Context, emoji_url: String, msg: &Message) {
    let _ = msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|embed| {
            embed.image(emoji_url);
            embed.colour(0x36393F);
            embed.author(|a| {
                if let Some(avatar_url) = &msg.author.avatar_url() {
                    a.icon_url(avatar_url);
                }
                a.name(&msg.author.name);
                return a;
            });
            return embed;
        })
    });
}

pub fn bigify_emoji(ctx: &Context, msg: &Message) {
    let emoji_db = get_local_emojis();
    let mut ftype = "png";
    let cdn = "https://cdn.discordapp.com/emojis/";
    let nitromatch = Regex::new(r"^<(a?):([^:<>]+?):(\d+)>$").unwrap();
    let plebmatch = Regex::new(r"^:(.+?):$").unwrap();
    if nitromatch.is_match(&msg.content) {
        if let Some(groups) = nitromatch.captures(&msg.content) {
            let caps: Vec<&str> = groups.iter().map(|x| x.unwrap().as_str()).collect();
            if msg.guild_id.is_some() {
                msg.delete(&ctx.http).expect("Could not delete message");
            }
            if caps[1] == "a" {
                ftype = "gif"
            }
            let emoji_url = format!("{}{}.{}?v=1", cdn, caps[3], ftype);
            let emoji_comparable = format!("{}: {}.{}", caps[2], caps[3], ftype);
            if !emoji_db.contains(&emoji_comparable) {
                emoji_dbadd(caps[2].to_string(), caps[3].to_string());
            }
            post_emoji(&ctx, emoji_url, &msg);
        }
    } else if plebmatch.is_match(&msg.content) {
        if let Some(groups) = plebmatch.captures(&msg.content) {
            let caps: Vec<&str> = groups.iter().map(|x| x.unwrap().as_str()).collect();
            let guilds = &ctx.cache.read().guilds;
            for (_, guild) in guilds {
                let emojis = &guild.read().emojis;
                for (emoji_id, emoji) in emojis {
                    if caps[1] == emoji.name {
                        if msg.guild_id.is_some() {
                            msg.delete(&ctx.http).expect("Could not delete message");
                        }
                        if emoji.animated {
                            ftype = "gif"
                        }
                        let emoji_url = format!("{}{}.{}?v=1", cdn, emoji_id, ftype);
                        post_emoji(&ctx, emoji_url, &msg);
                        return;
                    }
                }
            }
        }
    }
}
