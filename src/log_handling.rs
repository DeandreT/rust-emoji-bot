use chrono::Local;
use serenity::{client::Context, model::channel::Message};
use std::{
    fs::{create_dir, OpenOptions},
    io::{prelude::*, LineWriter},
    path::Path,
};

pub fn log_info(info: String) {
    let timestamp = Local::now();
    let local_date = timestamp.format("%Y-%m-%d");
    let local_time = timestamp.format("%H:%M:%S");
    let info_log = OpenOptions::new()
        .create(true)
        .append(true)
        .open(format!("emoji-logs/{}-info.log", local_date))
        .expect("Couldn't open info log file");
    let mut line = LineWriter::new(info_log);
    line.write_all(format!("{}|{}\n", local_time, info).as_bytes())
        .expect("Couldn't write to info log file");
}

pub fn log_message(ctx: &Context, msg: &Message) {
    let timestamp = Local::now();
    let local_date = timestamp.format("%Y-%m-%d");
    let local_time = timestamp.format("%H:%M:%S");
    if let Some(guild) = msg.guild(&ctx.cache) {
        let guild_name = &guild.read().name;
        let guild_dir = format!("server-logs/{}", guild_name);
        if !Path::new(&guild_dir).exists() {
            create_dir(&guild_dir).expect("Couldn't create guild directory");
        }
        let server_log = OpenOptions::new()
            .create(true)
            .append(true)
            .open(format!("{}/{}-#{}.log", &guild_dir, local_date, guild_name))
            .expect("Couldn't open server log");
        let mut line = LineWriter::new(server_log);
        line.write_all(format!("{}|{}: {}\n", local_time, msg.author.name, msg.content).as_bytes())
            .expect("Couldn't write to server log file");
    }
}
