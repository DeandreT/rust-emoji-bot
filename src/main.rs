use serenity::{
    prelude::*,
    framework::standard::StandardFramework,
    model::{channel::Message, gateway::Ready},
};
use std::{collections::HashSet, env};

mod bot_commands;
mod emoji_handling;
mod log_handling;

struct Handler;
impl EventHandler for Handler {
    fn ready(&self, _: Context, ready: Ready) {
        log_handling::log_info(format!(
            "Logged in as {}: {}",
            ready.user.name, ready.user.id
        ));
    }
    fn message(&self, ctx: Context, msg: Message) {
        log_handling::log_message(&ctx, &msg);
        emoji_handling::bigify_emoji(&ctx, &msg);
    }
}

fn main() {
    kankyo::load(false).expect("Failed to load .env file");
    let token = env::var("DISCORD_TOKEN").expect("Token not found in environment");
    let mut client = Client::new(&token, Handler).expect("Error creating client");
    let (owners, _) = match client.cache_and_http.http.get_current_application_info() {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);
            (owners, info.id)
        }
        Err(e) => panic!("Could not access application info: {:?}", e),
    };

    client.with_framework(
        StandardFramework::new()
            .configure(|c| c.prefix("!").owners(owners))
            .before(|_, msg, command_name| {
                println!("Got command '{}' by user '{}'",
                         command_name, msg.author.name);
                true})
            .group(&bot_commands::GENERAL_GROUP)
            .group(&bot_commands::OWNER_GROUP),
    );

    if let Err(why) = client.start() {
        println!("Client error: {:?}", why);
    }
}
